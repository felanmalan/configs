" WOW SUCH COOL .vimrc MAN!


" PLUGINS ---------------------------------------------------------------- {{{

call plug#begin('~/.vim/plugged')

" Themes
Plug 'colepeters/spacemacs-theme.vim'
Plug 'sainnhe/gruvbox-material'
Plug 'phanviet/vim-monokai-pro'
Plug 'flazz/vim-colorschemes'
Plug 'chriskempson/base16-vim'
Plug 'gruvbox-community/gruvbox'


" Addons
" Plug 'dpelle/vim-languagetool'
Plug 'preservim/nerdtree'
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'
Plug 'junegunn/goyo.vim'

call plug#end()

" }}}


" SETTINGS ------------------------------------------------------- {{{

colorscheme slate
colorscheme gruvbox-material

" Detects filtype
filetype on
filetype plugin on
filetype indent on

syntax on

set number
set cursorline

set textwidth=50
set smartindent
set spell
set undolevels=1000

set shiftwidth=4
set tabstop=4
set expandtab
set wrap

set scrolloff=15

set incsearch
set ignorecase
set showmatch
set hlsearch

set showcmd
set showmode

" Enable auto completion menu after pressing TAB.
set wildmenu
set wildmode=list:longest

" There are certain files that we would never want to edit with Vim.
" Wildmenu will ignore files with these extensions.
set wildignore=*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx

" }}}


" MAPPINGS --------------------------------------------------------------- {{{

inoremap jj <esk>

map j gj
map k gk

" }}}


" VIMSCRIPT -------------------------------------------------------------- {{{

" This will enable code folding.
" Use the marker method of folding.
augroup filetype_vim
    autocmd!
    autocmd FileType vim setlocal foldmethod=marker
augroup END

" More Vimscripts code goes here.

" }}}


" STATUS LINE ------------------------------------------------------------ {{{

set statusline=

set statusline+=\ %F\ %M\ %Y\ %R
set statusline+=%=
set statusline+=\ ascii:\ %b\ hex:\ 0x%B\ row:\ %l\ col:\ %c\ percent:\ %p%%

set laststatus=2

" }}}

