# Aliases

# Distrobox
alias arch="distrobox enter --root archlinux"

# Dnf aliases
alias sdi="sudo dnf install"
alias sdr="sudo dnf remove"
alias update="sudo dnf update"

# Apt aliases
#alias sai="sudo apt install"
#alias sar="sudo apt remove"
#alias sau="sudo apt update && sudo apt upgrade"
#alias sal="sudo apt list"
#alias sas="sudo apt search"

# Snap aliases 
#alias ssi="sudo snap install"
#alias ssr="sudo snap remove"
#alias sss="sudo snap search"
#alias ssl="sudo snap list"

# Pacman / Yay
#alias sps="sudo pacman -S"
#alias spr="sudo pacman -R"
#alias update="sudo pacman -Syyu"
#alias y="yay"
#alias ys="yay -S"

# Ls to exa
#alias ls="exa -xlF --icons"
#alias la="exa -axlF --icons"


# Variables
set -Ux EDITOR 'vim'
set -Ux VISUAL 'vim'

# Vim 
fish_vi_key_bindings

# Functions
function fish_greeting
	
end

# starship init fish | source

